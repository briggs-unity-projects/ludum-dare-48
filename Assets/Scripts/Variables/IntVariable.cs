using UnityEngine;

namespace Variables
{
    [CreateAssetMenu]
    public class IntVariable : ScriptableObject
    {
#if UNITY_EDITOR
        [Multiline] public string developerDescription = "";
#endif

        public int value;

        public void SetValue(int value)
        {
            this.value = value;
        }

        public void SetValue(IntVariable value)
        {
            this.value = value.value;
        }

        public void ApplyChanges(int amount)
        {
            value += amount;
        }

        public void ApplyChanges(IntVariable amount)
        {
            value += amount.value;
        }
    }
}